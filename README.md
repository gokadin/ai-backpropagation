# Backpropagation algorithm

Backpropagation is a technique used to teach a neural network that has at least one hidden layer. 

## This is part 2 of a series of github repos on neural networks

- [part 1 - linear associative network](https://github.com/gokadin/ai-linear-associative-network)
- part 2 - backpropagation (**you are here**)

## Table of Contents

- [Theory](#theory)  
  - [Introducing the perceptron](#introducing-the-perceptron)
  - [Backpropagation](#backpropagation)
    - [The forward pass](#the-forward-pass)
- [Code example](#code-example)
- [References](#references)

## Theory

### Introducing the perceptron

A perceptron is the same as our artificial neuron from part 1 of this series, expect that it has an activation threshold. 

![perceptron](readme-images/perceptron.jpg)

#### Activation functions

If <img src="/tex/22a1bca1370e6ec943222dc2ea608065.svg?invert_in_darkmode&sanitize=true" align=middle width=73.66339529999999pt height=24.657735299999988pt/> then typical activation functions are:

- Sigmoid

<p align="center"><img src="/tex/dfc80d3d1825da740f2f6b787495bc67.svg?invert_in_darkmode&sanitize=true" align=middle width=115.01144159999998pt height=18.312383099999998pt/></p>

- ReLU or rectified linear unit

<p align="center"><img src="/tex/26fea42205a1ecb142c6ba3cb3ba73bf.svg?invert_in_darkmode&sanitize=true" align=middle width=100.80487889999999pt height=16.438356pt/></p>

- tanh

<p align="center"><img src="/tex/4660ad0287e8a2fe5ffefc23983877df.svg?invert_in_darkmode&sanitize=true" align=middle width=86.7257853pt height=16.438356pt/></p>

### Backpropagation

The backpropagation algorith is used to train artificial neural networks, more specifically those with more than two layers. 

It's using a forward pass to compute the outputs of the network, calculates the error and then goes backwards towards the input layer to update each weight based on the error gradient. 

#### The forward pass

...

## Code example

under construction...

## References

- Artificial intelligence engines by James V Stone (2019)